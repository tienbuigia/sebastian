#!/usr/bin/env python3

import speech_recognition as sr
import pyttsx3
import os
import datetime
import webbrowser as wb

ears = sr.Recognizer()
sebastian = pyttsx3.init()  # sebastian born


def get_command():
    with sr.Microphone() as source:
        ears.pause_threshold = 2
        ears.adjust_for_ambient_noise(source)
        audio = ears.listen(source)
    try:
        command = ears.recognize_google(audio)
        print('Master: ' + command)
    except sr.UnknownValueError:
        print("It seems your highness's words were too much for your servant.")
        command = str(input('Your command is: '))
    except sr.RequestError as e:
        print('My lord. The organization has failed your trust; {0}'.format(e))
    return command


def speak(words):
    print(words)
    sebastian.say(words)
    sebastian.runAndWait()


def welcome():
    speak('Sebastian, at your service!')


def time():
    Time = datetime.datetime.now().strftime('%I:%M:%p')
    speak('It is: ' + Time)


if __name__ == '__main__':
    welcome()
    while True:
        query = get_command().lower()
        if 'open video' in query:
            os.system(
                "mpv /home/art/vid/meaningful/'Addiction [70dplWEKqow].webm'"
            )
        elif 'time' in query:
            time()
        elif 'youtube' in query:
            speak('What do you want to consume this time?')
            item = get_command().lower()
            url = f'https://www.youtube.com/search?q={item}'
            wb.get().open(url)
            speak(f'Here is {item} on Youtube')
        elif 'search' in query:
            speak('What are you seeking, my lord?')
            item = get_command().lower()
            url = f'https://search.brave.com/search?q={item}'
            wb.get().open(url)
            speak(f'Here is {item} on the Wired')
        elif 'quit' in query:
            speak('Yes, my highness!')
            quit()
        else:
            print(
                'Young master. Forgive me.\n'
                'I have been unable to understand you this time.'
            )
    os.exit()
