from tkinter import Tk, ttk, N, E, W, S, StringVar, simpledialog
import speech_recognition as sr
import pyttsx3
import os
import datetime
import webbrowser as wb
from threading import Thread

ears = sr.Recognizer()
sebastian = pyttsx3.init()  # sebastian born


class App:
    def __init__(self, root):
        root.title('assistant')

        # frame
        mainframe = ttk.Frame(root, padding='3 3 12 12')
        mainframe.grid(column=0, row=0, sticky=(N, E, S, W))
        # widgets
        self.dialog = StringVar()
        ttk.Label(mainframe, textvariable=self.dialog, width=30).grid(
            column=0, row=1, sticky=(W, E), columnspan=2
        )

        # entry
        # self.input = StringVar()
        # input_entry = ttk.Entry(mainframe, textvariable=self.input, width=30)
        # input_entry.grid(column=0, row=2, sticky=(W, E))
        # ttk.Button(mainframe, text='Enter', command=self.send).grid(
        #     column=1, row=2
        # )

        # polish
        for child in mainframe.winfo_children():
            child.grid_configure(padx=5, pady=5)
        # input_entry.focus()
        # root.bind('<Return>', self.send)

        ttk.Button(mainframe, text='Start', command=self.threading).grid(
            column=0, row=2
        )
        ttk.Button(mainframe, text='Quit', command=self.quit).grid(
            column=1, row=2
        )

    # def send(self, *args):
    #     self.write('\n' + self.input.get())
    #     self.input.set('')

    def get_command(self):
        with sr.Microphone() as source:
            ears.pause_threshold = 2
            ears.adjust_for_ambient_noise(source)
            audio = ears.listen(source)
        try:
            command = ears.recognize_google(audio)
            # print('Master: ' + command)
            self.write('\nMaster: ' + command)
        except sr.UnknownValueError:
            self.write('\nI failed to understand you.')
            # command = str(input('Your command is: '))
            command = simpledialog.askstring(
                'get_command', 'Your command is: '
            )
        except sr.RequestError as e:
            print('The recognize_google has failed; {0}'.format(e))
        return command

    def write(self, words):
        self.dialog.set(self.dialog.get() + words)

    def clean(self):
        self.dialog.set('')

    def speak(self, words):
        self.write('\nAssistant: ' + words)
        sebastian.say(words)
        sebastian.runAndWait()

    def quit(self, *args):
        self.speak("Yes, my highness!")
        root.destroy()
        quit()

    def run(self):
        while True:
            query = self.get_command().lower()
            if 'video' in query:
                os.system(
                    "mpv /home/art/vid/meaningful/'Addiction [70dplWEKqow].webm'"
                )
            elif 'time' in query:
                Time = datetime.datetime.now().strftime('%I:%M:%p')
                self.speak('It is: ' + Time)
            elif 'youtube' in query:
                self.speak('What do you want to consume this time?')
                item = self.get_command().lower()
                url = f'https://www.youtube.com/search?q={item}'
                wb.get().open(url)
                self.speak(f'Here is {item} on Youtube')
            elif 'search' in query:
                self.speak('What are you seeking, my lord?')
                item = self.get_command().lower()
                url = f'https://search.brave.com/search?q={item}'
                wb.get().open(url)
                self.speak(f'Here is {item} on the Wired')
            elif 'quit' in query:
                # self.speak('Yes, my highness!')
                self.quit()
            else:
                self.write(
                    'I have been unable to understand you this time.'
                )
            # self.clean()

    def threading(self, *args):
        t1 = Thread(target=self.run)
        self.speak('Your assistant, ready!')
        t1.start()


# main driver
root = Tk()
myapp = App(root)
root.mainloop()
