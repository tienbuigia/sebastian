# **sebastian** agent

Using [pyttsx3](https://pypi.org/project/pyttsx3/)
and [SpeechRecognition](https://pypi.org/project/SpeechRecognition/) libraries
to build a simple assistant works with voices.

inspired by [Codexplore](https://github.com/CodexploreRepo/python-youtube-tutorials/blob/master/projects/Virtual_Assistant_Friday.py).

## license

sebastian - tien
Copyright (C) 2022 tien <tien.2nghin@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
